import { ImageSourcePropType } from "react-native";
import { ExamType } from "../enums/examTypes";


function getImageFromExamType(type : ExamType) : ImageSourcePropType {
  let image = 0;
  switch (type) {
    case ExamType.Math:
      image = require('../assets/templogo_math.png');
      break;
    case ExamType.Read:
      image = require('../assets/templogo_read.png');
      break;
    case ExamType.Write:
      image = require('../assets/templogo_read.png');
      break;
    }
  return image;
}

export { getImageFromExamType };