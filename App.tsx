import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import HomeChild from './components/HomeChild';
import HomeParent from './components/HomeParent';
import Login from './components/Login';
import RecoverPassword from './components/RecoverPassword';
import Register from './components/Register';
import { RootStackParamList } from './components/RootStackParamList';

const Stack = createStackNavigator<RootStackParamList>();

export default function App() {
  return (
    <NavigationContainer>
        <Stack.Navigator 
        initialRouteName="Login"
        >
          <Stack.Screen
            name="Login"
            component={Login}
          />
          <Stack.Screen
            name="Register"
            component={Register}
          />
          <Stack.Screen
            name="HomeParent"
            component={HomeParent}
          />
          <Stack.Screen
            name="HomeChild"
            component={HomeChild}
          />
          <Stack.Screen
            name="RecoverPassword"
            component={RecoverPassword}
          />

        </Stack.Navigator>
    </NavigationContainer>
  );
}
