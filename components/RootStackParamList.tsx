export type RootStackParamList = {
  Login: undefined;
  Register: undefined;
  HomeParent: undefined;
  HomeChild: undefined;
  RecoverPassword: undefined;
};