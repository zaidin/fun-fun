import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import { Image, StyleSheet, TextInput, TouchableOpacity, View, Text, ImageBackground } from 'react-native';
import { RootStackParamList } from './RootStackParamList';

type authScreenProp = StackNavigationProp<RootStackParamList, 'Login'>;

export default function Login() {
  const navigation = useNavigation<authScreenProp>();
  const [userName, onChangeUserName] = React.useState("");
  const [password, onChangePassword] = React.useState("");
  
  return (
    <>
      <ImageBackground source={require('../assets/backgroundChildren.jpg')} style={{height: "110%", width:"100%", top:-80, opacity: 0.4, position: "absolute"}}>
      </ImageBackground>
      <View style={styles.container}>
          <TextInput
            style={styles.text}
            onChangeText={onChangeUserName}
            value={userName}
            placeholder="Nom de usuari"
          />
          <TextInput
            style={styles.text}
            onChangeText={onChangePassword}
            value={password}
            secureTextEntry={true}
            placeholder="Contrasenya"
          />
          <TouchableOpacity
            style={styles.button}
            // onPress={() => {navigation.navigate('HomeParent')}}>
            onPress={() => {navigation.navigate('HomeChild')}}>
              <View>
                <Text style={styles.buttonText}>Iniciar sessió</Text>
              </View>
          </TouchableOpacity>
          <View style={styles.links}>
            <TouchableOpacity
              style={styles.link}
              onPress={() => {navigation.navigate('Register')}}>
                <Text style={styles.linkText}>Registrar-se</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.link}
              onPress={() => {navigation.navigate('RecoverPassword')}}>
                <Text style={styles.linkText}>Recuperar contrasenya</Text>
            </TouchableOpacity>
          </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: -110,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
    paddingBottom: 20,
    flex: 1
  },
  image: {
    height: 180,
    width: 300,
    marginLeft: 50
  },
  text: {
    height: 40,
    margin: 5,
    borderWidth: 1,
    padding: 10,
    backgroundColor: '#cccccc',
    width: "50%"
  },
  button: {
    height: 40,
    margin: 5,
    borderWidth: 1,
    padding: 10,
    backgroundColor: '#3b8ce3',
    width: "50%",
    borderRadius: 10
  },
  buttonText: {
    color: '#ffffff'
  },
  links: {
    flexShrink: 10,
    width: "50%"
  },
  link: {
    left: 0,
    padding: 3
  },
  linkText: {
    fontSize: 12,
    color: "#0623ff"
  }
});
