import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { baseProps } from 'react-native-gesture-handler/lib/typescript/handlers/gestureHandlers';
import Item, { Props } from './FlatListItems/ActivitiesDetails';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    image: require('../assets/girl-1.png'),
    title: 'First Item',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    image: require('../assets/girl-2.png'),
    title: 'Second Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    image: require('../assets/boy-1.png'),
    title: 'Third Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    image: require('../assets/boy-2.png'),
    title: 'Third Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    image: require('../assets/girl-1.png'),
    title: 'Third Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    image: require('../assets/girl-2.png'),
    title: 'Third Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    image: require('../assets/boy-1.png'),
    title: 'Third Item',
  },
];

export default function HomeParent() {
  return (
      <View>
          <FlatList
            data={DATA}
            renderItem={({ item }) => <Item name={item.image} image={item.image} nameSecondElement={item.title} imageSecondElement={item.image} />}
          />
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: -110,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
    paddingBottom: 20,
    flex: 1
  }
});
