import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { ExamType } from '../enums/examTypes';
import ExamDetails from './FlatListItems/ExamDetails';


const DATA = {
  name: "Nom",
  image: require('../assets/girl-1.png'),
  progress: 50,
  imageTest: require('../assets/templogo_math.png')
};

const DATA_EXAMS = [
  {
    type: ExamType.Math,
    title: "Sumes i restes"
  },
  {
    type: ExamType.Read,
    title: "Lectura català"
  },
  {
    type: ExamType.Write,
    title: "Escriptura català"
  },
  {
    type: ExamType.Math,
    title: "Multiplicacions"
  },
  {
    type: ExamType.Math,
    title: "Sumes tres nombres"
  },
  {
    type: ExamType.Math,
    title: "Sumes i restes"
  },
  {
    type: ExamType.Read,
    title: "Lectura català"
  },
  {
    type: ExamType.Write,
    title: "Escriptura català"
  },
  {
    type: ExamType.Math,
    title: "Multiplicacions"
  },
  {
    type: ExamType.Math,
    title: "Sumes tres nombres"
  },
  {
    type: ExamType.Math,
    title: "Sumes i restes"
  },
  {
    type: ExamType.Read,
    title: "Lectura català"
  },
  {
    type: ExamType.Write,
    title: "Escriptura català"
  },
  {
    type: ExamType.Math,
    title: "Multiplicacions"
  },
  {
    type: ExamType.Math,
    title: "Sumes tres nombres"
  },
  {
    type: ExamType.Math,
    title: "Sumes i restes"
  },
  {
    type: ExamType.Read,
    title: "Lectura català"
  },
  {
    type: ExamType.Write,
    title: "Escriptura català"
  },
  {
    type: ExamType.Math,
    title: "Multiplicacions"
  },
  {
    type: ExamType.Math,
    title: "Sumes tres nombres"
  },
];


export default function HomeChild() {
  return (
      <View>
          <View style={styles.header}>
            <Text style={styles.textHeader}>{DATA.name}</Text>
            <View style={[styles.imageWrapper, styles.marginRight10]}>
              <Image source={DATA.image} style={styles.imageHeader} />
            </View>
          </View>
          <View style={styles.progress}>
            <View style={[styles.imageWrapperTest, styles.marginLeft10]}>
              <Image source={DATA.imageTest} style={styles.imageTest} />
            </View>
            <View style={styles.progressWrapper}>
              <View style={styles.progressBar}>
                <View style={[styles.innerProgressBar, {width: DATA.progress + "%"}]} />
                <Text style={styles.textProgress}>{DATA.progress}%</Text>
              </View>
            </View>
          </View>
          
            <FlatList
              data={DATA_EXAMS}
              renderItem={({item}) => <ExamDetails title={item.title} key={item.title} examType={item.type}></ExamDetails>}
            />
          
      </View>
  );
}

const styles = StyleSheet.create({
  header: {
    width: "100%",
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingTop: 10
  },
  textHeader: {
    flex: 4,
    textAlign: 'right',
    fontSize: 20,
    paddingRight: 10
  },
  imageWrapper: {
    width : 50,
    height: 50,
    borderRadius: 10,
    backgroundColor: 'rgba(0, 125, 30, 0.3)',
    justifyContent : 'center',
    alignItems: 'center'
  },
  imageHeader: {
    flex: 1,
    height: 40,
    width : 40,
    resizeMode: 'contain'
  },
  marginRight10: {
    marginRight: 10
  },
  marginLeft10: {
    marginLeft: 10
  },
  imageWrapperTest: {
    width : 110,
    height: 110,
    borderRadius: 10,
    justifyContent : 'center',
    alignItems: 'center'
  },
  imageTest: {
    flex: 1,
    height: 100,
    width : 100,
    resizeMode: 'contain'
  },
  progress: {
    paddingTop: 10,
    paddingBottom: 10
  },
  progressWrapper: {
    alignItems: 'center'
  },
  progressBar: {
    borderWidth: 2,
    borderRadius: 10,
    borderColor: 'rgba(61, 126, 255, 0.5)',
    width: "90%",
    height: 30,
    justifyContent: 'center',
    padding: 2,
  },
  innerProgressBar: {
    height: 24,
    backgroundColor: 'rgba(61, 126, 255, 0.5)',
    borderRadius: 9,
    maxWidth: "100%"
  },
  textProgress: {
    position: 'absolute',
    left: "48%",
    fontWeight: "bold"
  }
});
