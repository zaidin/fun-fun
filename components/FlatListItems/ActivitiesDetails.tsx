import React from 'react';
import { Image, ImageSourcePropType, StyleSheet, Text, View } from 'react-native';

export type Props = {
  name: string;
  image: ImageSourcePropType;
  nameSecondElement: string;
  imageSecondElement: ImageSourcePropType;
};

export default function ActivitiesDetails(props: Props) {
  return (
      <View style={styles.container}>
          <View style={[styles.imageContainer, styles.imageContainerLeft]}>
            <View style={styles.imageWrapper}>
              <Image source={props.image} style={styles.image} />
            </View>
            <Text style={styles.text}>{props.name}</Text>
          </View>
          <View style={[styles.imageContainer, styles.imageContainerRight]}>
            <View style={styles.imageWrapper}>
              <Image source={props.imageSecondElement} style={styles.image} />
            </View>
            <Text style={styles.text}>{props.nameSecondElement}</Text>
          </View>
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  image: {
    width: 100,
    height : 100,
    resizeMode: 'contain'
  },
  imageContainer: {
    width: "50%",
    alignItems: 'center',
    height: 150,
    justifyContent: 'center'
  },
  imageContainerLeft: {
    paddingLeft: "8%"
  },
  imageContainerRight: {
    paddingRight: "8%"
  },
  imageWrapper: {
    width : 110,
    height: 110,
    borderRadius: 10,
    backgroundColor: 'rgba(0, 125, 30, 0.3)',
    justifyContent : 'center',
    alignItems: 'center'
  },
  text: {
    paddingTop: 7
  }
});
