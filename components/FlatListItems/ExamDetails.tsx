import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { ExamType } from '../../enums/examTypes';
import { getImageFromExamType } from '../../utils/examUtils'

export type Props = {
  title: string;
  examType: ExamType;
};

export default function ExamDetails(props: Props) {
  return(
    <View style={styles.container}>
      <Image style={styles.image} source={getImageFromExamType(props.examType)} />
      <Text style={styles.text}>{props.title}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 5,
    flexDirection: 'row',
    alignItems: 'center'
  },
  image: {
    height: 50,
    width: 50,
    resizeMode: 'contain'
  },
  text: {
  }
});
